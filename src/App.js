import { Container } from 'react-bootstrap';
import { Route, Routes, useParams } from 'react-router-dom';
import './App.css';
import AddArticle from './pages/Article/AddArticle';
import EditArticle from './pages/Article/EditArticle';
import ViewArticle from './pages/Article/ViewArticle';
import AddCategory from './pages/Category/AddCategory';
import Categories from './pages/Category/Categories';
import EditCategory from './pages/Category/EditCategory';
import ViewCategory from './pages/Category/ViewCategory';
import HomeComponent from './pages/HomeComponent';
import NavbarComponent from './shared-layout/NavbarComponent';

function App() {

  return (
    <div>
      <NavbarComponent />
      <Container>
        <Routes>
          <Route path='/' element={<HomeComponent/>} />
          <Route path='/addarticle' element={<AddArticle />} />
          <Route path='/editarticle' element={<EditArticle />} />
          <Route path='/viewarticle' element={<ViewArticle />} />
          <Route path='/categories' element={<Categories/>} />
          <Route path='/viewcategory' element={<ViewCategory />} />
          <Route path='/editcategory' element={<EditCategory />} />
          <Route path='/addcategory' element={<AddCategory />} />
        </Routes>
      </Container>
    </div>
  )
}

export default App;
