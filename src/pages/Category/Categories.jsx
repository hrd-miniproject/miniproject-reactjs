import React, { useEffect, useState } from 'react'
import { Button, Card } from 'react-bootstrap'
import { NavLink, useNavigate } from 'react-router-dom'
import { api } from '../../Api'

export default function Categories() {

    const [cate, setCate] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        api.get('/category').then((r) => setCate(r.data.payload)).then(console.log("Get Category Success"))
    }, [])

    const onEdit = (item) => {
        navigate("/editcategory", { state: { ...item } });
    }

    const onViews = (item) => {
        navigate("/viewcategory", { state: { ...item } });
    }


    const handleDelete = (id) => {
        const newData = cate.filter(() => cate._id !== id)
        setCate(newData)
        api.delete(`/category/${id}`).then((r) => console.log("Post API work !", r))
        navigate('/categories')
    }

    return (
        <div>
            <div className='title' style={{ display: "flex" }}>
                <h3 style={{ flex: "1" }}>All Category</h3>
                <Button style={{ justifySelf: "flex-end" }} className='addArticle' as={NavLink} to='/addcategory' >New Category</Button>
            </div>
            <div className='home row'>
                {
                    cate.map((item, idx) =>
                        <Card key={idx} className='col-12 col-lg-3 col-md-3 col-sm-4 mt-4 card-holder' >
                            <Card className="mx-4 mt-2">
                                <Card.Body className='m-2'>
                                    <Card.Title className='card-title' style={{ textAlign: 'center' }} >{item.name}</Card.Title>
                                    <Button onClick={() => onEdit(item)} className='mybtn my-1 w-100' variant="primary">Edit</Button>
                                    <Button onClick={() => onViews(item)} className='mybtn my-1 w-100' variant="warning">View</Button>
                                    <Button onClick={() => handleDelete(item._id)} className='mybtn my-1 w-100' variant="danger">Delete</Button>
                                </Card.Body>
                            </Card>
                        </Card>
                    )
                }
            </div>
        </div>
    )
}
