import React, { useEffect, useState } from 'react'
import { Button, Container, Row } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'

export default function ViewCategory() {

  const navigate = useNavigate()

  const [id, setId] = useState('')
  const [title, setTitle] = useState('')

  const location = useLocation();
  const oldData = location.state

  useEffect(() => {
    setId(oldData._id)
    setTitle(oldData.title)
  }, [location])

  return (
    <div>
      <Container className="my-2">
        <Row className="view-card p-3">
          <Button
            onClick={() => navigate("/categories")}
            style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"
          >
            Back
          </Button>
          <div className="" style={{ display: "flex", margin: 5, height: "500px" }} >
              <h4 className="p-3">{oldData?.name} </h4>
              <p style={{ position: "relative", flex: 1 }} className="p-3">
                {oldData?.description}
              </p>
              <p className="p-3">
                {oldData?.published ? "Published" : "UnPublish"}
              </p>
          </div>
        </Row>
      </Container>
    </div>
  )
}
