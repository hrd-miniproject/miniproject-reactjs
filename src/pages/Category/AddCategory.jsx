import React from 'react'
import { useState } from 'react';
import { Button, Form, Image } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { api } from '../../Api';

export default function AddCategory() {

    const [name, setName] = useState('')

    const handleSubmit = () => {
        api.post('/category', { name })
            .then((res) => console.log(res.data.message))
    }

    return (
        <div>
            <Form>
                <Form.Group className="mb-3 " controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control onChange={(e) => setName(e.target.value)} type="title" placeholder="Enter title" />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>

                <Button variant="danger" type="submit" className='mt-4 col-4' style={{ marginRight:"30px" }} >
                    Cancel
                </Button>

                <Button as={NavLink} to='/categories' onClick={handleSubmit} variant="primary" type="submit" className='mt-4 col-4'>
                    Submit
                </Button>

            </Form>
        </div>
    )
}
