import React, { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'
import { api } from '../../Api'

export default function EditCategory() {

  const [name, setName] = useState("");
  const [id, setId] = useState();

  const navigate = useNavigate()

  const handleName = (e) => {
    setName(e.target.value)
  }

  const handleSubmit = (e) => {
    api.put('/category/' + id, { name })
    .then((res) => console.log(res))
    navigate('/categories')
  }

  const location = useLocation();
  const oldData = location.state

  useEffect(() => {
    setId(oldData._id)
    setName(oldData.name)
  }, [location])

  return (
    <div>
      <Form>
        <Form.Group className="mb-3 " controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control onChange={handleName} value={name} type="title" placeholder="Enter title" />
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>
        <Button onClick={() => navigate('/categories')} variant="danger" type="submit" className='mt-4 col-4' style={{ marginRight: "30px" }} >
          Cancel
        </Button>
        <Button onClick={(e) => handleSubmit(e)} variant="primary" type="submit" className='mt-4 col-4'>
          Submit
        </Button>

      </Form>
    </div>
  )
}
