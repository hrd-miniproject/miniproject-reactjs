import { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { NavLink, useNavigate } from 'react-router-dom'
import { api } from '../Api'

export default function HomeComponent() {

    const [deta, setDeta] = useState([])

    useEffect(() => {
        api.get('/articles').then((r) => setDeta(r.data.payload)).then(console.log("Get API Success"))
    }, [])

    const navigate = useNavigate();

    const onUpdate = (item) => {
        navigate("/editarticle", { state: { ...item } });
    }

    const onView = (item) => {
        navigate("/viewarticle", { state: { ...item } })
    }

    const handleDelete = (idx) => {
        const newData = deta.filter(() => deta._id !== idx)
        setDeta(newData)
        api.delete(`/articles/${idx}`).then((r) => console.log("Post API work !", r.data.payload))
    }

    return (
        <div>
            <div className='title' style={{ display: "flex" }}>
                <h3 style={{ flex: "1" }}>All Article</h3>
                <Button style={{ justifySelf: "flex-end" }} className='addArticle' as={NavLink} to='/addarticle' >New Article</Button>
            </div>
            <div className='home row pt-4' >
                {
                    deta.map((item, idx) =>
                        <Card key={idx} className='col-12 col-lg-2 col-md-4 col-sm-6 my-2 card-holder' >
                            <Card className="inner-card">
                                <div className='img-holder'>
                                    <Card.Img className='imgcards' variant="top" src={item?.image ?? "https://www.alpiend.com/admin/assets/images/default.png"} />
                                </div>
                                <Card.Body className='m-2'>
                                    <Card.Title className='card-title' >{item?.title ?? "No Title"}</Card.Title>
                                    <Card.Text className='desc'>
                                        {item?.description ?? "No Info"}
                                    </Card.Text>
                                    <Button onClick={() => onUpdate(item)} className='mybtn my-1 w-100' variant="primary">Edit</Button>
                                    <Button onClick={() => onView(item)} className='mybtn my-1 w-100' variant="warning">View</Button>
                                    <Button as={NavLink} to='/' onClick={() => handleDelete(item._id)} className='mybtn my-1 w-100' variant="danger">Delete</Button>
                                </Card.Body>
                            </Card>
                        </Card>
                    )
                }
            </div>

        </div>
    )
}
