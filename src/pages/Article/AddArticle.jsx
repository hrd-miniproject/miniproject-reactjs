import React from 'react'
import { useState } from 'react';
import { Button, Form, Image } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { api } from '../../Api';

export default function AddArticle() {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState("")
    const [published, setPublished] = useState(true)
    const [image, setImage] = useState('')
    const [imageUrl, setImageUrl] = useState()
    const [category, setCategory] = useState('')

    const handleTitle = (e) => {
        setTitle(e.target.value)
    }

    const handleDesc = (e) => {
        setDescription(e.target.value)
    }

    const handlePublished = (e) => {
        setPublished(e.target.checked)
    }

    const handleImage = (e) => {
        const formData = new FormData();
        formData.append("image", e.target.files[0]);
        api.post("/images", formData).then((print) => {
            setImage(print.data.payload.url)
        })
        setImageUrl(URL.createObjectURL(e.target.files[0]))
    }

    const handleSubmit = (event) => {
        api.post('/articles', { title, description, published, image, category })
            .then((res) => console.log(res))
    }

    return (
        <div>
            <Form>
                <Form.Group className="mb-3 " controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control onChange={(e) => handleTitle(e)} type="title" placeholder="Enter title" />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control onChange={(e) => handleDesc(e)} type=" description" placeholder="Enter description" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox"
                        label="Is Published"
                        onChange={(e) => handlePublished(e)} />
                </Form.Group>
                <Image src={imageUrl ?? "https://www.alpiend.com/admin/assets/images/default.png"} className='preview-img' />
                <Form.Group controlId="formFileMultiple" className="mb-3 mt-3">
                    <Form.Control onChange={handleImage ?? "https://www.alpiend.com/admin/assets/images/default.png"} type="file" />
                </Form.Group>
                <div className='mt-4' style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end ", width: "100%" }}>
                    <Button variant="danger" type="submit" style={{ justifySelf: "flex-start", marginRight: "30px" }} >
                        Cancel
                    </Button>
                    <Button as={NavLink} to='/' onClick={(event) => handleSubmit(event)} variant="primary" type="submit" style={{ justifySelf: "flex-end", marginRight: "30px" }} >
                        Submit
                    </Button>
                </div>
            </Form>
        </div>
    )
}
