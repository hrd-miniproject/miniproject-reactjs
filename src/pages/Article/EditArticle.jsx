import React, { useEffect, useState } from 'react'
import { Button, Form, Image } from 'react-bootstrap'
import { useLocation, useNavigate} from 'react-router-dom'
import { api } from '../../Api'

export default function EditArticle() {

    const [id, setId] = useState()
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState("")
    const [isPublished, setPublished] = useState(true)
    const [image, setImage] = useState('')
    const [category, setCategory] = useState('')

    const navigate = useNavigate()

    const handleTitle = (e) => {
        setTitle(e.target.value)
    }

    const handleDesc = (e) => {
        setDescription(e.target.value)
    }

    const handlePublished = (e) => {
        setPublished(e.target.checked)
    }

    const handleImage = (e) => {
        const formData = new FormData();
        formData.append("image", e.target.files[0]);
        api.post("/images", formData).then((f) => {
            console.log(f.data.payload.url)
            setImage(f.data.payload.url)
        })
        setImage(URL.createObjectURL(e.target.files[0]))
    }

    const handleSubmit = () => {
        api.patch("/articles/" + id, { title, description, isPublished, image, category })
            .then((res) => console.log(res))
            navigate('/')
    }

    const location = useLocation();
    const oldData = location.state

    useEffect(() => {
        setId(oldData._id)
        setTitle(oldData.title)
        setDescription(oldData.description)
        setImage(oldData.image)
        setPublished(oldData.isPublished)
        setCategory(oldData.category)
    }, [location])

    return (
        <div>
            <Form>
                <Form.Group className="mb-3 " controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control onChange={(e) => handleTitle(e)} value={title} type="title" placeholder="Enter title" />
                    <Form.Text className="text-muted">
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control onChange={(e) => handleDesc(e)} value={description} type=" description" placeholder="Enter description" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox"
                        label="Is Published"
                        onChange={(e) => handlePublished(e)} />
                </Form.Group>
                <Image src={image ?? "https://www.alpiend.com/admin/assets/images/default.png"} className='preview-img' />
                <Form.Group controlId="formFileMultiple" className="mb-3 mt-3">
                    <Form.Control onChange={handleImage ?? "https://www.alpiend.com/admin/assets/images/default.png"} type="file" />
                </Form.Group>
                <div className='mt-4' style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end ", width: "100%" }}>
                    <Button variant="danger" type="submit" style={{ justifySelf: "flex-start", marginRight: "30px" }} >
                        Cancel
                    </Button>
                    <Button onClick={() => handleSubmit()} variant="primary" type="submit" style={{ justifySelf: "flex-end", marginRight: "30px" }} >
                        Submit
                    </Button>
                </div>
            </Form>

        </div>
    )
}
