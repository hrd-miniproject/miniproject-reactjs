import React, { useEffect, useState } from 'react'
import { Button, Card, Container, Form, Image, Row } from 'react-bootstrap'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'
import { api } from '../../Api'

export default function EditArticle() {

  const navigate = useNavigate()

  const [id, setId] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState("")
  const [isPublished, setPublished] = useState(true)
  const [image, setImage] = useState('')
  const [category, setCategory] = useState('')

  const location = useLocation();
  const oldData = location.state

  useEffect(() => {
    setId(oldData._id)
    setTitle(oldData.title)
    setDescription(oldData.description)
    setImage(oldData.image)
    setPublished(oldData.isPublished)
    setCategory(oldData.category)
  }, [location])

  return (
    <div>
      <Container className="my-2">
        <Row className="view-card p-3">
          <Button
            onClick={() => navigate("/")}
            style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"
          >
            Back
          </Button>
          <div className="" style={{ display: "flex", margin: 5, height: "500px" }} >
            <Image
              src={oldData.image ?? "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"}
              width="50%"
              height="100%"
              style={{ flex: 1, borderRadius: "20px", objectFit: "cover" }}
              className="" />
            <div
              style={{
                flex: 1,
                height: "100%",
                flexWrap: "wrap",
                width: "100vw",
                position: "relative",
                overflow: "hidden",
                wordBreak: "break-word",
              }} >
              <h4 className="p-3">Title : {oldData?.title} </h4>
              <p style={{ position: "relative", flex: 1 }} className="p-3">
                {oldData?.description}
              </p>
              <p className="p-3">
                {oldData?.published ? "Published" : "UnPublish"}
              </p>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  )
}
